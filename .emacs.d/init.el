(setq inhibit-startup-message t) ;; don't show splash screen
(setq visible-bell t) ;; flashes when the bell rings 

;; Line and Column number on modeline
(line-number-mode 1)
(setq column-number-mode t)

;; Display line numbers in every buffer on side
(global-display-line-numbers-mode 1)

;; automode
(add-to-list 'auto-mode-alist '("\\.org\\'" . org-mode))

;; set <TAB> length
(setq-default tab-width 3)

(menu-bar-mode -1) ;; disable menu bar
(tool-bar-mode -1) ;; disable tool bar
(scroll-bar-mode -1) ;; disable scroll bar

;; Set theme
(load-theme 'modus-vivendi t)

;; Transparent Background
(set-frame-parameter (selected-frame) 'alpha '(85 . 100))
(add-to-list 'default-frame-alist '(alpha . (85 . 100)))

;; Inesert bracket pairs
(electric-pair-mode 1)
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
	'(slime geiser-guile cider clojure-mode web-mode modus-themes)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
