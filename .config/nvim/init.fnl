;(set vim.g.mapleader " ")
;(set vim.g.maplocalleader " ")

(set vim.opt.showcmd true)
(set vim.opt.laststatus 2)
(set vim.opt.autowrite true)
(set vim.opt.autoread true)
(set vim.opt.cursorline true)

(set vim.opt.number true)
(set vim.opt.shiftwidth 3)
(set vim.opt.tabstop 3)

(vim.cmd "syntax on")
