# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
#if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    # if [ -f "$HOME/.bashrc" ]; then
	 # . "$HOME/.bashrc"
    # fi
#fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

# set PATH so it includes user's private bin if it exists
export PATH="$HOME/.local/bin:$PATH"
export EDITOR=vi

# XDG DIRS
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"
export XDG_STATE_HOME="$HOME/.local/state"

# GUIX CURRENT BIN
export PATH="$HOME/.config/guix/current/bin:$PATH"

if [ -e /home/zephyr/.nix-profile/etc/profile.d/nix.sh ]; then . /home/zephyr/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer

# ONYX RUNTIME and ONYX EXEC in PATH
export ONYX_PATH="$HOME/tools/onyx-linux-ovm-amd64"
export PATH="$ONYX_PATH/bin:$PATH"

# JAVA Config
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="${XDG_CONFIG_HOME}"/java
export JAVA_HOME="${HOME}/tools/jdk-21.0.5+11/"
export PATH="${JAVA_HOME}/bin:${PATH}"

# V Lang Stuff
if [ ! -d "${XDG_CACHE_HOME}/vmodules/" ] ; then
 mkdir --parents "${XDG_CACHE_HOME}/vmodules/"
fi
export VMODULES="${XDG_CACHE_HOME}/vmodules"